exports.handler = async (event, context, callback) => {
  try {
    context.callbackWaitsForEmptyEventLoop = false;

    console.log('GET delete system-user called');
    return callback(null, {});
  } catch (err) {
    console.log('Error delete system-user:', err);
    return callback(null, err.message);
  }
};
