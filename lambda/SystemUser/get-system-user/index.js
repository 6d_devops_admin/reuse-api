exports.handler = async (event, context, callback) => {
  try {
    context.callbackWaitsForEmptyEventLoop = false;

    console.log('GET get system-user called');
    return callback(null, {});
  } catch (err) {
    console.log('Error get system-user:', err);
    return callback(null, err.message);
  }
};
