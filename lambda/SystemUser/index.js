const deletePostSystemUser = require('./delete-system-user');
const getSystemUser = require('./get-system-user');
const getSystemUserById = require('./get-system-user-by-id');
const getPatchSystemUser = require('./patch-system-user');
const getPostSystemUser = require('./post-system-user');

exports.handler = (event, context, callback) => {
  try {
    switch (event.httpMethod) {
      case 'GET':
        if (event.pathParameters && event.pathParameters.id) {
          return getSystemUserById.handler(event, context, callback);
        }
        return getSystemUser.handler(event, context, callback);
      case 'POST':
        return getPostSystemUser.handler(event, context, callback);
      case 'PATCH':
        return getPatchSystemUser.handler(event, context, callback);
      case 'DELETE':
        return deletePostSystemUser.handler(event, context, callback);
      default:
        console.log('Unsupported http method for system-user API');
        return callback(null, 'Unsupported method');
    }
  } catch (err) {
    return callback(null, err.message);
  }
};
